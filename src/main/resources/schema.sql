DROP TABLE IF EXISTS orders;

DROP SEQUENCE IF EXISTS seq1;

CREATE SEQUENCE seq1 START WITH 1;

CREATE TABLE orders (
    id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('seq1')
    , orderNumber VARCHAR(255) NOT NULL
    , orderRows JSON -- it's easier to store json as is, since end data is not know, still can be queried
);