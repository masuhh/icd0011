package utils;

import lombok.AllArgsConstructor;
import lombok.Value;

@AllArgsConstructor
@Value
public class ConnectionInfo {
    private final String url;
    private final String user;
    private final String pass;
}
