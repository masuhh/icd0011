package exservlet;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;

class CustomMapper {
    private ObjectMapper mapper;

    /*
    I'm a comment?
     */
    protected CustomMapper() {
        this.mapper = new ObjectMapper();
    }

    /*
    Avoiding mistakes, apparently :/
     */
    protected ObjectMapper getMapper() {
        mapper.findAndRegisterModules();
        mapper.configure(JsonGenerator.Feature.WRITE_NUMBERS_AS_STRINGS, true);
        return mapper;
    }
}
