package exservlet;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import exservlet.dao.OrderDAO;
import exservlet.model.Order;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet("api/orders")
public class Orders extends HttpServlet {
    //private static Long uid = 1L;
    private Map<Long, Order> ordersList = new HashMap<>();
    private ObjectMapper mapper = new CustomMapper().getMapper();
    private OrderDAO orderDAO;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String inputAsString = Util.readStream(request.getInputStream());

        DataSource dataSource = (DataSource) request.getServletContext().getAttribute("Database");
        orderDAO = new OrderDAO(dataSource);

        Order order;
        String attribute = "";

        try {
            attribute = request.getAttribute("orderNumber").toString();
        } catch (Exception e) {
            log(e.getMessage());
        }


        if (attribute.isEmpty()) {
            order = mapper.readValue(inputAsString, Order.class);

            try {
                JsonNode node = mapper.readValue(inputAsString, JsonNode.class);

                JsonNode orderRowNode = node.get("orderRows");

                if (orderRowNode != null) {
                    //constructing types
//                    MapType mapType = mapper.getTypeFactory().constructMapType(HashMap.class, String.class, String.class);
//                    CollectionType mapCollectionType = mapper.getTypeFactory().constructCollectionType(List.class, mapType);
//
//                    ArrayList<HashMap<String, String>> test = mapper.readValue(orderRowNode.toString(), mapCollectionType);
//                    //[{"itemName":"CPU","quantity":2,"price":100},{"itemName":"Motherboard","quantity":3,"price":60}]
//                    order.setOrderRows(test);
                    String orderRows = mapper.writeValueAsString(orderRowNode);
                    order.setOrderRows(orderRows);
                }

            } catch (Exception e) {
                log(e.getMessage());
            }
        }
        else {
            order = Order.builder()
                    .orderNumber(request.getAttribute("orderNumber").toString())
                    .build();
        }

        order = orderDAO.insertOrder(order);

        ordersList.put(order.getId(), order);

        if (attribute.isEmpty()) {
            String payload = mapper.writeValueAsString(order);
            response.setHeader("Content-Type", "application/json");
            response.getWriter().print(payload);
        } else {
            response.getWriter().print(order.getId());
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        DataSource dataSource = (DataSource) request.getServletContext().getAttribute("Database");
        orderDAO = new OrderDAO(dataSource);
        String json;

        try {

            Long orderId = Long.parseLong(request.getParameter("id"));
            Order order = orderDAO.getOrderById(orderId);
            json = new ObjectMapper().writeValueAsString(order);
        } catch (NumberFormatException e) {
            List<Order> orders = orderDAO.getAllOrders();
            json = mapper.writeValueAsString(orders);
        }


        response.setHeader("Content-Type", "application/json");
        response.getWriter().print(json);
    }

}
