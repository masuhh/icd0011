package exservlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@MultipartConfig
@WebServlet("orders/form")
public class OrdersForm extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String inputAsString = Util.readStream(request.getInputStream());


//        if ((contentType != null) && contentType.startsWith("multipart/form-data")) // found out that actually we are working with x-www-form-urlencoded (from tests) :/
//        {
//            Collection<Part> parts = request.getParts();
//        }

        String[] values = inputAsString.split("=");

        String orderNumber = values[1];


        ServletContext application = getServletConfig().getServletContext();

        ServletContext context = application.getContext("/api");

        request.getSession().setAttribute("orderNumber", orderNumber);

        request.setAttribute("orderNumber", orderNumber);

        try {
            context.getRequestDispatcher("/api/orders").include(request, response);
        } catch (Exception e) {
           log(e.getMessage());
        }

        response.setHeader("Content-Type", "text/plain");
    }

}
