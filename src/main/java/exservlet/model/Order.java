package exservlet.model;

import lombok.*;


@Builder
@AllArgsConstructor
public class Order {

    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String orderNumber;

//    @Getter
//    @Setter
//    private List<HashMap<String, String>> orderRows;

    @Getter
    @Setter
    private String orderRows;

}
