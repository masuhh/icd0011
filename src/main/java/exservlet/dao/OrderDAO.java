package exservlet.dao;

import exservlet.model.Order;
import lombok.AllArgsConstructor;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public class OrderDAO {
    private DataSource dataSource;

    public Order insertOrder(Order order) {

        String sqlInsertOrder = "INSERT INTO orders (orderNumber, orderRows) " +
                "VALUES (?, to_json(?::json))";

        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sqlInsertOrder, new String[] {"id"})) {

            preparedStatement.setString(1, order.getOrderNumber());
            preparedStatement.setString(2, order.getOrderRows());

            preparedStatement.executeUpdate();

            ResultSet resultSet = preparedStatement.getGeneratedKeys();


            if (resultSet.next()) {
                order = Order.builder()
                        .id(resultSet.getLong("id"))
                        .orderNumber(order.getOrderNumber())
                        .orderRows(order.getOrderRows())
                        .build();
                //order.setId(resultSet.getLong("id"));

            } else {
                throw new SQLException("Something went wrong");
            }

            return order;

        } catch (SQLException e) {
            return null;
        }
    }

    public List<Order> getAllOrders() {
        List<Order> orders = new ArrayList<>();
        String sqlGetOrders = "SELECT id" +
                ", orderNumber" +
                ", orderRows" +
                " FROM orders";

        try(Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement()) {

            ResultSet resultSet = statement.executeQuery(sqlGetOrders);

            while (resultSet.next()) {
                Order order = Order.builder()
                        .id(resultSet.getLong("id"))
                        .orderNumber(resultSet.getString("orderNumber"))
                        .orderRows(resultSet.getString("orderRows"))
                        .build();
                orders.add(order);
            }
        } catch (SQLException e) {
            return null;
        }
        return orders;
    }

    public Order getOrderById (Long id) {
        String sqlGetById = "SELECT id" +
                ", orderNumber" +
                ", orderRows " +
                "FROM orders " +
                "WHERE id = ?";

        try(Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(sqlGetById)) {

            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return Order.builder()
                        .id(id)
                        .orderNumber(resultSet.getString("orderNumber"))
                        .orderRows(resultSet.getString("orderRows"))
                        .build();
            } else {
                return null;
            }
        } catch (SQLException e) {
            return null;
        }
    }
}
