package main;

import utils.ConnectionInfo;
import utils.DataSourceProvider;
import utils.DbUtil;
import utils.FileUtil;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.Statement;

@WebListener
public class AppContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ConnectionInfo connectionInfo = DbUtil.loadConnectionInfo();

        DataSourceProvider.setConnectionInfo(connectionInfo);
        DataSource dataSource = DataSourceProvider.getDataSource();

        ServletContext servletContext = servletContextEvent.getServletContext();
        servletContext.setAttribute("Database", dataSource);

        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()){

            String sqlSchemaCreation = FileUtil.readFileFromClasspath("schema.sql");
            statement.executeUpdate(sqlSchemaCreation);

        } catch (Exception e) {
           servletContext.log(e.getMessage());
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        DataSourceProvider.closePool();
    }
}
